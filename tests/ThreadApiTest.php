<?php

namespace SamMessenger\Tests;

use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAdmin\Testing\ApiTest;
use Totem\SamMessenger\App\Model\Thread;

class ThreadApiTest extends ApiTest
{

    use AttachRoleToUserTrait;

    protected string $endpoint = 'messenger';
    protected string $model = Thread::class;

    protected array $withoutFields = [
        'subject'
    ];

    protected function createModel(array $attributes = []): Thread
    {
       return factory($this->model)->create($attributes);
    }

    public function testEndpointDeleteDestroy() : void
    {
        $model = $this->createModel();

        $this->call('DELETE', "/api/$this->endpoint/$model->slug")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function testEndpointGetOne() : void
    {
        $model = $this->createModel();

        $this->call('GET', "/api/$this->endpoint/$model->slug")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function testGetOne() : void
    {
        $model = $this->createModel();

        $response = $this->json('GET', "/api/$this->endpoint/$model->slug");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testDestroy() : void
    {
        $model = $this->createModel();

        $response = $this->json('DELETE', "/api/$this->endpoint/$model->slug");

        $response->assertOk();
    }

    public function testWrongActionToPatch() : void
    {
        $model = $this->createModel();

        $response = $this->json('PATCH', "/api/$this->endpoint/$model->id", [ 'action' => 'bla bla bla' ]);
        $response
            ->assertJson([
                'error' => true
            ])
            ->assertStatus(422)
        ;
    }

}
