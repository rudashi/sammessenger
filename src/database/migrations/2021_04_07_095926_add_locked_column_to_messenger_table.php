<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLockedColumnToMessengerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try{
            if (!Schema::hasColumn('message_threads', 'locked')) {
                Schema::table('message_threads', function (Blueprint $table) {
                    $table->string('locked')->nullable()->after('archived');
                });
            }
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        if (Schema::hasColumn('message_threads', 'locked')) {
            Schema::table('message_threads', function (Blueprint $table) {
                $table->dropColumn('locked');
            });
        }
    }
}
