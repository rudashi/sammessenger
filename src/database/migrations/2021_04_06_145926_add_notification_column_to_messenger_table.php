<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationColumnToMessengerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try{
            if (!Schema::hasColumn('messages', 'notify')) {
                Schema::table('messages', function (Blueprint $table) {
                    $table->json('notify')->nullable()->after('body');
                });
            }
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        if (Schema::hasColumn('messages', 'notify')) {
            Schema::table('messages', function (Blueprint $table) {
                $table->dropColumn('notify');
            });
        }
    }
}
