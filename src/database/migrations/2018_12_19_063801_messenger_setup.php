<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MessengerSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up() : void
    {
        try{
            Schema::create('message_threads', static function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->boolean('starred')->default(0);
                $table->string('slug')->unique();
                $table->string('subject');
                $table->string('model_id')->nullable();
                $table->string('model_type')->nullable();
                $table->integer('max_participants')->nullable();
                $table->timestamp('start_date')->nullable();
                $table->timestamp('end_date')->nullable();
                $table->string('avatar')->nullable();
                $table->tinyInteger('archived')->default(0);
                $table->softDeletes();
            });

            Schema::create('messages', static function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->integer('thread_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->string('type');
                $table->text('body');
                $table->softDeletes();

                $table->foreign('thread_id')->references('id')->on('message_threads')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

            Schema::create('message_users', static function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->integer('thread_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->timestamp('last_read')->nullable();
                $table->softDeletes();

                $table->foreign('thread_id')->references('id')->on('message_threads')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamMessenger\Database\Seeds\PermissionSeeder::class
        ]);

        Artisan::call('db:seed', [
            '--class' => \Totem\SamMessenger\Database\Seeds\MessengerSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down() : void
    {
        Schema::dropIfExists('message_users');
        Schema::dropIfExists('messages');
        Schema::dropIfExists('message_threads');

        (new \Totem\SamMessenger\Database\Seeds\PermissionSeeder)->down();
    }
}
