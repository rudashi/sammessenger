<?php

use Illuminate\Support\Str;
use Totem\SamMessenger\App\Model\Thread;
use Totem\SamMessenger\App\Model\Message;
use Totem\SamMessenger\App\Model\Participant;
use Faker\Generator as Faker;

/**  @var $factory \Illuminate\Database\Eloquent\Factory */

$factory->define(Thread::class, static function(Faker $faker) {

    $subject = $faker->unique()->sentence;

    return [
        'slug' => Str::slug($subject),
        'subject' => $subject,
    ];
});

$factory->define(Message::class, static function(Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 10),
        'type' => 'text',
        'body' => $faker->realText(),
    ];
});

$factory->define(Participant::class, static function(Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 10),
    ];
});

$factory->afterCreating(Thread::class, static function(Thread $thread) {
    $thread->attachMessages(factory(Message::class, 10)->make());
    $thread->attachParticipants(factory(Participant::class, 10)->make());
});
