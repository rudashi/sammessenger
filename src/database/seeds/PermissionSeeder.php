<?php

namespace Totem\SamMessenger\Database\Seeds;

use Illuminate\Database\Seeder;
use Totem\SamAcl\Database\PermissionTraitSeeder;

class PermissionSeeder extends Seeder
{

    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        return [
            [
                'slug' => 'messenger.view',
                'name' => 'Can View Messenger',
                'description' => 'Can view messages list',
            ],
            [
                'slug' => 'messenger.delete',
                'name' => 'Can Delete Threads',
                'description' => 'Can delete threads',
            ],
            [
                'slug' => 'messenger.edit',
                'name' => 'Can Edit Messages',
                'description' => 'Can modify message in thread',
            ],
        ];
    }

}
