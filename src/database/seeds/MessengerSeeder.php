<?php

namespace Totem\SamMessenger\Database\Seeds;

use Totem\SamMessenger\App\Model\Thread;
use Illuminate\Database\Seeder;

class MessengerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        if (config('app.env') !== 'production') {
            factory(Thread::class, 10)->create();
        }
    }
}
