<?php

namespace Totem\SamMessenger\App\Resources;

use Totem\SamCore\App\Resources\FileCollection;
use Totem\SamUsers\App\Resources\UserResource;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property \Totem\SamMessenger\App\Model\Message $resource
 */
class MessageResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'            => $this->resource->id,
            'date'          => $this->resource->created_at->format('d.m.Y'),
            'created_at'    => $this->resource->created_at->toDateTimeString(),
            'updated_at'    => $this->resource->updated_at->toDateTimeString(),
            'created_date'  => $this->resource->created_date,
            'updated_date'  => $this->resource->updated_date,
            'created_time'  => $this->resource->created_at->toTimeString(),
            'updated_time'  => $this->resource->updated_at->toTimeString(),
            'thread_id'     => $this->resource->thread_id,
            'user_id'       => $this->resource->user_id,
            'body'          => $this->resource->body,
            'notify'        => $this->resource->notify,
            'thread'        => ThreadResource::make($this->whenLoaded('thread')),
            'user'          => UserResource::make($this->whenLoaded('user')),
            'participants'  => ParticipantResource::collection($this->whenLoaded('participants')),
            'files'         => FileCollection::make($this->whenLoaded('files')),
        ];
    }

}
