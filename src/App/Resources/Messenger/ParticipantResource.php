<?php

namespace Totem\SamMessenger\App\Resources\Messenger;

use Totem\SamCore\App\Resources\ApiResource;

class ParticipantResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'        => $this->resource->user->id,
            'fullname'  => $this->resource->user->fullname,
            'image'     => $this->resource->user->image,
        ];
    }

}
