<?php

namespace Totem\SamMessenger\App\Resources\Messenger;

use Illuminate\Support\Collection;
use Totem\SamMessenger\App\Model\Message;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property null|Collection $resource
 */
class PingResource extends ApiResource
{

    public bool $preserveKeys = true;

    public function toArray($request) : array
    {
        if (!$this->resource) {
            return [];
        }

        return [$this->resource->mapToGroups(function(Message $item) {
            return [$item->created_at->format('d.m.Y') => [
                'id'            => $item->id,
                'created_time'  => $item->created_at->toTimeString(),
                'body'          => $item->body,
                'is_mine'       => $item->is_mine,
                'user'          => $item->user_id,
                'displayed'     => null,
            ]];
        })];
    }

}
