<?php

namespace Totem\SamMessenger\App\Resources\Messenger;

use Totem\SamCore\App\Resources\ApiResource;

class MessageResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'            => $this->resource->id,
            'created_time'  => $this->resource->created_time,
            'body'          => $this->resource->body,
            'is_mine'       => $this->resource->is_mine,
            'user'          => $this->resource->user,
            'displayed'     => $this->resource->displayed,
            'notify'        => $this->resource->notify,
        ];
    }

}
