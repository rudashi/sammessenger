<?php

namespace Totem\SamMessenger\App\Resources\Messenger;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property \Totem\SamMessenger\App\Model\Thread $resource
 */
class SimpleThreadResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'subject'           => $this->resource->subject,
            'model_id'          => $this->resource->model_id,
            'model_type'        => $this->resource->model_type,
            'avatar'            => $this->resource->avatar,
            'locked'            => $this->resource->locked,
            'unread'            => $this->resource->isUnread(auth()->id()),
            'messages'          => MessagesCollection::collection($this->resource->messages),
            'participants'      => ParticipantResource::collection($this->resource->participants),
        ];
    }

}
