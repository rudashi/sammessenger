<?php

namespace Totem\SamMessenger\App\Resources\Messenger;

use Totem\SamCore\App\Resources\ApiCollection;

class MessagesCollection extends ApiCollection
{

    public $collects = MessageResource::class;

}
