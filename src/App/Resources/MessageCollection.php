<?php

namespace Totem\SamMessenger\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class MessageCollection extends ApiCollection
{

    public $collects = MessageResource::class;

}
