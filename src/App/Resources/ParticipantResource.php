<?php

namespace Totem\SamMessenger\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamUsers\App\Resources\UserResource;

class ParticipantResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'            => $this->id,
            'thread_id'     => $this->thread_id,
            'user_id'       => $this->user_id,
            'last_read'     => $this->last_read instanceof \Carbon\Carbon ? $this->last_read->toDateTimeString() : $this->last_read,
            'thread'        => ThreadResource::make($this->whenLoaded('thread')),
            'user'          => UserResource::make($this->whenLoaded('user')),
        ];
    }

}
