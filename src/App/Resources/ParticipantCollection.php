<?php

namespace Totem\SamMessenger\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class ParticipantCollection extends ApiCollection
{

    public $collects = ParticipantResource::class;

}
