<?php

namespace Totem\SamMessenger\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class ThreadCollection extends ApiCollection
{

    public $collects = ThreadResource::class;

}
