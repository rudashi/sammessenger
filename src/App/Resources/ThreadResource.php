<?php

namespace Totem\SamMessenger\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamUsers\App\Resources\UserResource;

class ThreadResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'                => $this->id,
            'starred'           => $this->starred,
            'slug'              => $this->slug,
            'subject'           => $this->subject,
            'model_id'          => $this->model_id,
            'model_type'        => $this->model_type,
            'max_participants'  => $this->max_participants,
            'start_date'        => $this->start_date,
            'end_date'          => $this->end_date,
            'avatar'            => $this->avatar,
            'unread'            => $this->resource->isUnread(),
            'participants'      => ParticipantResource::collection($this->whenLoaded('participants')),
            'users'             => UserResource::collection($this->whenLoaded('users')),
            'messages'          => MessageResource::collection($this->whenLoaded('messages')),
            'latest_message'    => MessageResource::make($this->whenLoaded('latestMessage')),
        ];
    }

}
