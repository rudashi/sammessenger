<?php

namespace Totem\SamMessenger\App\Repositories;

use Illuminate\Http\Request;
use Totem\SamMessenger\App\Model\Thread;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamMessenger\App\Repositories\Contracts\MessagesRepositoryInterface;
use Totem\SamMessenger\App\Repositories\Contracts\MessengerRepositoryInterface;
use Totem\SamMessenger\App\Repositories\Contracts\ParticipantsRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Thread model
 */
class MessengerRepository extends BaseRepository implements MessengerRepositoryInterface
{

    private MessagesRepositoryInterface $messagesRepository;
    private ParticipantsRepositoryInterface $participantsRepository;

    public function __construct(ParticipantsRepositoryInterface $participantsRepository, MessagesRepositoryInterface $messagesRepository)
    {
        $this->messagesRepository = $messagesRepository;
        $this->participantsRepository = $participantsRepository;

        parent::__construct();
    }

    public function model(): string
    {
        return Thread::class;
    }

    private function getThreadsQuery(): \Illuminate\Database\Eloquent\Builder
    {
        return $this->model->with(['latestMessage', 'participant'])->latest();
    }

    public function allThreads(array $columns = ['*']) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->getThreadsQuery()->get($columns);
    }

    public function allUserThreads(int $userId, array $columns = ['*']) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->latest()
            ->whereHas('participants', static function(\Illuminate\Database\Eloquent\Builder $query) use ($userId) {
                $query->where('user_id', $userId);
            })->get($columns);
    }

    public function getCurrentUserThreads(array $columns = ['*']) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->allUserThreads(auth()->id(), $columns);
    }

    public function allUserThreadWithNewMessages(int $userId, array $columns = ['*']) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->getThreadsQuery()
            ->whereHas('participant', static function(\Illuminate\Database\Eloquent\Builder $query) use ($userId) {
                $query->where('user_id', $userId)
                    ->where(static function(\Illuminate\Database\Eloquent\Builder $query) {
                        $query->where('message_threads.updated_at', '>', 'last_read')
                            ->orWhereNull('last_read');
                    });
            })->get($columns);
    }

    public function findThreadWithRelations(string $slug = null, array $relationships = [], array $columns = ['*']) : Thread
    {
        if ($slug === null) {
            throw new RepositoryException( __('No id have been given.') );
        }

        /** @var null|Thread $data */
        $data = $this->model->with($relationships)->where('slug', $slug)->first($columns);

        if ($data === null) {
            throw new RepositoryException( __('Given slug :code is invalid or thread not exist.', ['code' => $slug]), 404);
        }

        return $data;
    }

    public function findThread(string $slug, array $columns = ['*']) : Thread
    {
        return $this->findThreadWithRelations($slug, [], $columns);
    }

    public function findThreadWithMessages(string $slug, array $columns = ['*']) : Thread
    {
        return $this->findThreadWithRelations($slug, ['messages'], $columns);
    }

    public function getThreadParticipants(Thread $thread) : \Illuminate\Database\Eloquent\Collection
    {
        return $thread->participants;
    }

    public function getThreadUsers(Thread $thread) : \Illuminate\Database\Eloquent\Collection
    {
        return $thread->users;
    }

    public function getThreadUsersWithoutCurrentUser(Thread $thread) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->getThreadUsersWithoutUserIds($thread, [ auth()->id() ]);
    }

    public function getThreadUsersWithoutUserIds(Thread $thread, $userId = null) : \Illuminate\Database\Eloquent\Collection
    {
        if (!is_array($userId)) {
            $userId = [ $userId ];
        }

        return $thread->users->reject(static function($item) use ($userId) {
            return array_search($item->id, $userId, true);
        });
    }

    public function storeThread(Request $request, string $slug = null) : Thread
    {
        $userId = auth()->id();

        if ($slug === null) {
            $thread = $this->model;
            $thread->subject = $request->input('subject');
            $thread->slug    = $request->input('slug');
        } else {
            $thread = $this->findThread($slug);
        }
        $thread->fill([
            'model_id'          => $request->input('model_id'),
            'model_type'        => $request->input('model_type'),
            'start_date'        => $request->input('start_date'),
            'end_date'          => $request->input('end_date'),
            'max_participants'  => $request->input('max_participants'),
            'avatar'            => $request->input('avatar'),
            'starred'           => $request->input('starred', 0),
        ]);

        $thread->save();

        if ($request->has('message')) {
            $this->messagesRepository->add($thread, $request->input('message'), $userId);
        }

        if ($request->has('participants')) {
            $this->participantsRepository->addMany($thread, $request->input('participants'));
        }

        if ($thread->getParticipantFromUser($userId) === null) {
            $this->participantsRepository->add($thread, $userId, new \Carbon\Carbon());
        }

        return $thread;
    }

    public function markThreadAsRead(Thread $thread, int $userId) : Thread
    {
        $thread->markAsRead($userId);

        return $thread;
    }

    public function markThreadAsUnread(Thread $thread, int $userId) : Thread
    {
        $thread->markAsUnread($userId);

        return $thread;
    }

    public function markAsArchived(string $slug): Thread
    {
        $model = $this->findThread($slug);
        $model->archived = 1;
        $model->save();

        return $model;
    }

    public function destroy(string $slug): Thread
    {
        $thread = $this->findThread($slug);

        $this->delete($thread->id);

        return $thread;
    }

    public function fetchThread(string $slug): Thread
    {
        $thread = $this->findThreadWithRelations($slug, ['messages', 'participants.user']);
        $last_message = $thread->messages->last();
        $thread->messages = $thread->messages->mapToGroups(static function(\Totem\SamMessenger\App\Model\Message $item) use ($thread, $last_message) {
            if ($item->id === $last_message->id) {
                $displayed = $thread->participants->where('last_read', '>=', $item->created_at)->pluck('user_id');
                $displayed = $displayed->isNotEmpty() ? $displayed : null;
            }
            return [
                $item->created_at->format('d.m.Y') => (object) [
                    'id'            => $item->id,
                    'created_time'  => $item->created_at->toTimeString(),
                    'body'          => $item->body,
                    'is_mine'       => $item->is_mine,
                    'user'          => $item->user_id,
                    'displayed'     => $displayed ?? null,
                    'notify'        => $item->notify,
                ]
            ];
        });
        return $thread;
    }
}
