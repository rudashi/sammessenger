<?php

namespace Totem\SamMessenger\App\Repositories;

use Illuminate\Http\Request;
use Totem\SamMessenger\App\Services\PingService;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamMessenger\App\Model\Thread;
use Totem\SamMessenger\App\Model\Message;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamMessenger\App\Repositories\Contracts\MessagesRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Message model
 */
class MessagesRepository extends BaseRepository implements MessagesRepositoryInterface
{

    public function model(): string
    {
        return Message::class;
    }

    public function createModel(Request $request, int $userId = null): Message
    {
        $message                = $this->makeModel();
        $message->fill([
            'body'      => $request->input('message'),
            'type'      => $request->input('type', 'text'),
            'user_id'   => $userId ?? auth()->id(),
            'notify'    => $request->input('notify'),
        ]);

        return $message;
    }

    public function add(Thread $thread, string $body, int $userId = null) : Message
    {
        $message            = $this->makeModel();
        $message->fill([
            'body'      => $body,
            'type'      => 'text',
            'user_id'   => $userId ?? auth()->id(),
        ]);

        $thread->attachMessage($message);

        return $message;
    }

    public function replace(Thread $thread, int $id, string $body) : Message
    {
        $message = $this->findInThread($thread, $id);

        if ($message->user_id !== auth()->id()) {
            throw new RepositoryException( __('Unable to modify message of other user.'), 422);
        }

        $message->body = $body;
        $message->save();

        return $message;
    }

    public function destroy(Thread $thread, int $id) : Message
    {
        $message = $thread->detachMessage($id);

        if ($message !== null) {
            return $message;
        }

        throw new RepositoryException( __('Given id :code is invalid or element not exist.', ['code' => $id]), 404);
    }

    public function findInThread(Thread $thread, int $id, array $columns = ['*']) : Message
    {
        if ($id === 0) {
            throw new RepositoryException( __('No id have been given.') );
        }

        /** @var Message $data */
        $data = $thread->messages()->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException( __('Given id :code is invalid or element not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function ping($uuid = null, string $timestamp = null, ?array $slugs = []): \Illuminate\Support\Collection
    {
        return (new PingService($this->model, $uuid, $timestamp))
            ->setSlugs($slugs ?? [])
            ->run();
    }
}
