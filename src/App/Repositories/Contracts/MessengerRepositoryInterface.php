<?php

namespace Totem\SamMessenger\App\Repositories\Contracts;

use Totem\SamMessenger\App\Model\Thread;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface MessengerRepositoryInterface extends RepositoryInterface
{

    public function allThreads(array $columns = ['*']): \Illuminate\Database\Eloquent\Collection;

    public function allUserThreads(int $userId, array $columns = ['*']): \Illuminate\Database\Eloquent\Collection;

    public function getCurrentUserThreads(array $columns = ['*']): \Illuminate\Database\Eloquent\Collection;

    public function allUserThreadWithNewMessages(int $userId, array $columns = ['*']): \Illuminate\Database\Eloquent\Collection;

    public function findThreadWithRelations(string $slug = null, array $relationships = [], array $columns = ['*']): Thread;

    public function findThread(string $slug, array $columns = ['*']): Thread;

    public function findThreadWithMessages(string $slug, array $columns = ['*']): Thread;

    public function getThreadParticipants(Thread $thread): \Illuminate\Database\Eloquent\Collection;

    public function getThreadUsers(Thread $thread): \Illuminate\Database\Eloquent\Collection;

    public function getThreadUsersWithoutUserIds(Thread $thread, $userId = null): \Illuminate\Database\Eloquent\Collection;

    public function getThreadUsersWithoutCurrentUser(Thread $thread): \Illuminate\Database\Eloquent\Collection;

    public function storeThread(\Illuminate\Http\Request $request, string $slug = null): Thread;

    public function markThreadAsRead(Thread $thread, int $userId): Thread;

    public function markAsArchived(string $slug): Thread;

    public function markThreadAsUnread(Thread $thread, int $userId): Thread;

    public function destroy(string $slug): Thread;

    public function fetchThread(string $slug): Thread;

}