<?php

namespace Totem\SamMessenger\App\Repositories\Contracts;

use Totem\SamMessenger\App\Model\Message;
use Totem\SamMessenger\App\Model\Thread;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface MessagesRepositoryInterface extends RepositoryInterface
{

    public function findInThread(Thread $thread, int $id, array $columns = ['*']) : Message;

    public function add(Thread $thread, string $body, int $userId = null) : Message;

    public function replace(Thread $thread, int $id, string $body) : Message;

    public function destroy(Thread $thread, int $id) : Message;

}