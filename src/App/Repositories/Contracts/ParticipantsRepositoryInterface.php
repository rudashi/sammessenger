<?php

namespace Totem\SamMessenger\App\Repositories\Contracts;

use Totem\SamMessenger\App\Model\Thread;
use Totem\SamMessenger\App\Model\Participant;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface ParticipantsRepositoryInterface extends RepositoryInterface
{

    public function add(Thread $thread, int $userId = null, \Carbon\Carbon $lastRead = null) : Participant;

    public function addMany(Thread $thread, array $userIds) : \Illuminate\Support\Collection;

    public function destroy(Thread $thread, int $id) : Participant;

}