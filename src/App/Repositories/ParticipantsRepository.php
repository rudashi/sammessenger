<?php

namespace Totem\SamMessenger\App\Repositories;

use Carbon\Carbon;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamMessenger\App\Model\Participant;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamMessenger\App\Model\Thread;
use Totem\SamMessenger\App\Repositories\Contracts\ParticipantsRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Participant model
 */
class ParticipantsRepository extends BaseRepository  implements ParticipantsRepositoryInterface
{

    public function model(): string
    {
        return Participant::class;
    }

    private function makeParticipant(int $userId = null, Carbon $lastRead = null): Participant
    {
        $participant = $this->makeModel();
        $participant->user_id   = $userId ?? auth()->id();
        $participant->last_read = $lastRead;

        return $participant;
    }

    public function add(Thread $thread, int $userId = null, Carbon $lastRead = null) : Participant
    {
        return $thread->attachParticipant($this->makeParticipant($userId, $lastRead));
    }

    public function addMany(Thread $thread, array $userIds) : \Illuminate\Support\Collection
    {
        return collect($userIds)->map(function ($userId) use ($thread) {
            return $this->add($thread, $userId);
        });
    }

    public function destroy(Thread $thread, int $id) : Participant
    {
        $participant = $thread->detachParticipant($id);

        if ($participant !== null) {
            return $participant;
        }

        throw new RepositoryException(  __('Given id :code is invalid or element not exist.', ['code' => $id]), 404);
    }

}