<?php

namespace Totem\SamMessenger\App\Observers;

use Illuminate\Support\Facades\Notification;
use Totem\SamMessenger\App\Model\Message;
use Totem\SamMessenger\App\Notifications\MessageCreated;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;

class MessageObserver
{

    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function creating(Message $message): void
    {
        if ($message->notify !== null && count($message->notify) > 0) {
            $message->notify = $this->userRepository->findByIds($message->notify)->pluck('email');
        }
    }

    public function created(Message $message): void
    {
        if ($message->notify !== null && count($message->notify) > 0) {
            Notification::route('mail', $message->notify)->notify(new MessageCreated($message));
        }
    }

}
