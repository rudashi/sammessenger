<?php

namespace Totem\SamMessenger\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class MessageRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'message' => 'required',
            'type' => 'string|nullable',
            'notify' => 'array',
            'notify.*' => 'int',
        ];
    }

    public function attributes() : array
    {
        return [
            'message' => __('Body'),
        ];
    }

}
