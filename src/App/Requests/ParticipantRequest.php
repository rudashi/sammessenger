<?php

namespace Totem\SamMessenger\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class ParticipantRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'user_id' => 'required',
        ];
    }

    public function attributes() : array
    {
        return [
            'user_id' => __('User ID'),
        ];
    }

}
