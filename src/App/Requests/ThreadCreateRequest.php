<?php

namespace Totem\SamMessenger\App\Requests;

use Illuminate\Support\Str;
use Totem\SamCore\App\Requests\BaseRequest;

class ThreadCreateRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'slug'              => 'unique:message_threads,slug',
            'subject'           => 'required',
            'start_date'        => 'nullable',
            'end_date'          => 'nullable',
            'max_participants'  => 'integer|nullable',
            'avatar'            => 'nullable',
            'starred'           => 'integer',
        ];
    }

    public function attributes() : array
    {
        return [
            'subject' => __('Subject'),
        ];
    }

    protected function prepareForValidation() : void
    {
        $this->merge(['slug' => Str::slug($this->subject)]);
    }
}
