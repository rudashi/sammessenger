<?php

namespace Totem\SamMessenger\App\Services;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Totem\SamMessenger\App\Model\Message;

class PingService
{

    private Message $model;
    private $user_id;
    private ?string $timestamp;
    private ?array $slugs;

    public function __construct(Message $model, $user_id = null, string $timestamp = null)
    {
        $this->model = $model;
        $this->user_id = $user_id;
        $this->timestamp = $timestamp;
    }

    public function setSlugs(array $slugs): PingService
    {
        $this->slugs = $slugs;

        return $this;
    }

    public function run(): Collection
    {
        if (count($this->slugs) > 0) {
            return $this->getWithMessages();
        }
        return $this->getWithoutMessages();
    }

    private function query(): Collection
    {
        return $this->model
            ::with('thread:id,slug')
            ->whereHas('participants', function(Builder $query) {
                $query->where('user_id', $this->user_id)
                    ->where(function(Builder $query) {
                        $query->where('updated_at', '>', 'last_read')
                            ->orWhereNull('last_read');
                    });
            })
            ->whereDate('created_at', '>=', $this->timestamp)
            ->get(['id', 'created_at', 'thread_id', 'user_id', 'body', 'type']);
    }

    private function getWithMessages(): Collection
    {
        return $this->query()
            ->groupBy('thread.slug')
            ->mapWithKeys(function(Collection $c, $k) {
                if (in_array($k, $this->slugs, true)) {
                    return [$k => $c];
                }
                return [$k => null];
            });
    }

    private function getWithoutMessages(): Collection
    {
        return $this->query()->pluck(0,'thread.slug');
    }
}
