<?php

namespace Totem\SamMessenger\App\Traits;

use Totem\SamMessenger\App\Model\Thread;

trait HasMessenger
{
    public function message_thread() : \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->morphOne(Thread::class, 'asdfg', 'model_type', 'model_id');
    }
}
