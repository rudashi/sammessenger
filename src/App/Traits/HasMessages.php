<?php

namespace Totem\SamMessenger\App\Traits;

use Totem\SamMessenger\App\Model\Message;
use Totem\SamMessenger\App\Model\Thread;

trait HasMessages
{

    public function threads() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Thread::class, 'message_users', 'user_id', 'thread_id');
    }

    public function messages() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Message::class);
    }

    public function threadsWithNewMessages() : \Illuminate\Database\Eloquent\Collection
    {
        return $this->threads()
            ->where(function (\Illuminate\Database\Eloquent\Builder $query) {
                $query->whereNull('last_read')
                    ->orWhere('message_threads.updated_at', '>', 'last_read');
            })->get();
    }

    public function threadsWithNewMessagesCount() : int
    {
        return $this->threadsWithNewMessages()->count();
    }

    public function unreadMessagesCount() : int
    {
        return $this->threadsWithNewMessagesCount();
    }
}
