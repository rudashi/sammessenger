<?php

namespace Totem\SamMessenger\App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Bus\Queueable;
use Totem\SamMessenger\App\Model\Message;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageCreated extends Notification implements ShouldQueue
{
    use Queueable;

    private Message $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function via(): array
    {
        return [
            'mail',
        ];
    }

    public function toMail(): MailMessage
    {
        $url = config('app.url').'/t/message/'.$this->message->thread->slug;
        $message = explode("\n", $this->message->body);

        $mail = new MailMessage;
        $mail->subject('[SAM] Nowa wiadomość - '.$this->message->thread->subject)
            ->greeting('Witaj,')
            ->line('Masz nową wiadomość.')
            ->line('Wspomniano o Tobie w ostatniej wiadomości.')
            ->line('')
        ;

        foreach ($message as $line) {
            $mail->line($line);
        }

        $mail->line('')
            ->action('Sprawdź SAMa', $url)
        ;
        return $mail;
    }

}
