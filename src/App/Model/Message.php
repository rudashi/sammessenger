<?php

namespace Totem\SamMessenger\App\Model;

use Totem\SamCore\App\Traits\HasFiles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCore\App\Repositories\Contracts\HasFilesInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Illuminate\Support\Carbon|null created_at
 * @property \Illuminate\Support\Carbon|null updated_at
 * @property \Illuminate\Support\Carbon|null deleted_at
 *
 * @property int id
 * @property int thread_id
 * @property int user_id
 * @property string type
 * @property string body
 * @property array|null notify
 *
 * @property string updated_date
 * @property string created_date
 * @property Thread thread
 * @property \Illuminate\Database\Eloquent\Collection|Participant participants
 * @property \App\User user
 * @property bool is_mine
 */
class Message extends Model implements HasFilesInterface
{
    use SoftDeletes,
        HasFiles;

    protected $casts = [
        'notify' => 'object',
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'updated_at',
            'deleted_at',
        ]);

        $this->fillable([
            'thread_id',
            'user_id',
            'type',
            'body',
            'notify',
        ]);

        $this->setTable('messages');

        parent::__construct($attributes);
    }

    public function path() : string
    {
        return 'messenger';
    }

    public function thread() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Thread::class, 'thread_id', 'id');
    }

    public function user() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id')->withTrashed();
    }

    public function participants() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Participant::class, 'thread_id', 'thread_id');
    }

    public function getCreatedDateAttribute() : string
    {
        if ($this->{$this->getCreatedAtColumn()}->diffInDays() <= 7) {
            return "{$this->{$this->getCreatedAtColumn()}->diffForHumans()}.";
        }
        return trim($this->{$this->getCreatedAtColumn()}->formatLocalized('%e %b %Y'));
    }

    public function getUpdatedDateAttribute() : string
    {
        if ($this->{$this->getUpdatedAtColumn()}->diffInDays() <= 1) {
            return "{$this->{$this->getUpdatedAtColumn()}->diffForHumans(null, true)}.";
        }

        if ($this->{$this->getUpdatedAtColumn()}->diffInDays() <= 7) {
            return "{$this->{$this->getUpdatedAtColumn()}->formatLocalized('%a')}.";
        }

        return trim($this->{$this->getUpdatedAtColumn()}->formatLocalized('%e %b'));
    }

    public function getIsMineAttribute(): bool
    {
        return $this->user_id === auth()->id();
    }

}
