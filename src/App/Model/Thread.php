<?php

namespace Totem\SamMessenger\App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Illuminate\Support\Carbon|null created_at
 * @property \Illuminate\Support\Carbon|null updated_at
 * @property int id
 * @property string slug
 * @property string subject
 * @property int|null model_id
 * @property string|null model_type
 * @property \Illuminate\Support\Carbon|null start_date
 * @property \Illuminate\Support\Carbon|null end_date
 * @property int|null max_participants
 * @property string|null avatar
 * @property string|null starred
 * @property int archived
 * @property bool locked
 * @property \Illuminate\Support\Carbon|null deleted_at
 * @property bool is_unread
 *
 * @property Message|null latestMessage
 * @property Participant|null participant
 * @property \Illuminate\Database\Eloquent\Collection|Message messages
 * @property \Illuminate\Database\Eloquent\Collection|Participant participants
 * @property \Illuminate\Database\Eloquent\Collection users
 */
class Thread extends Model
{
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'updated_at',
            'deleted_at',
        ]);

        $this->fillable([
            'subject',
            'model_id',
            'model_type',
            'start_date',
            'end_date',
            'max_participants',
            'avatar',
            'starred',
            'archived'
        ]);

        $this->addDateAttributesToArray([
            'start_date',
            'end_date',
        ]);

        $this->setTable('message_threads');

        parent::__construct($attributes);
    }

    public function model(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function messages() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Message::class, 'thread_id', 'id');
    }

    public function latestMessage() : \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Message::class, 'thread_id', 'id')->latest();
    }

    public function participants() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Participant::class, 'thread_id', 'id');
    }

    public function participant() : \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Participant::class, 'thread_id', 'id')->where('user_id', auth()->id());
    }

    public function users() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(config('auth.providers.users.model'), 'message_users', 'thread_id', 'user_id');
    }

    public function getAvatarAttribute(string $avatar = null) : ?string
    {
        if ($avatar !== null && is_file('storage/messenger/' . $avatar)) {
            return asset('storage/messenger/' . $avatar);
        }

        return $avatar;
    }

    public function getLatestMessageDateAttribute() : ?string
    {
        $message = $this->latestMessage;

        return $message->updated_date ?? null;
    }

    public function getLatestMessageBodyAttribute() : ?string
    {
        $message = $this->latestMessage;

        return $message->body ?? null;
    }

    public function getOwnerAttribute()
    {
        $message = $this->messages->last();

        return $message->user ?? null;
    }

    public function getLockedAttribute(?string $value): bool
    {
        if ($value) {
            return !in_array(auth()->id(), $this->fromJson($value), true);
        }
        return false;
    }

    public function isUnread(int $userId = null) : bool
    {
        $participant = $this->getParticipantFromUser($userId);

        if ($participant !== null) {
            if ($participant->last_read === null || $this->{$this->getUpdatedAtColumn()}->gt($participant->last_read)) {
                return true;
            }
        }
        return false;
    }

    public function markAsRead(int $userId = null, bool $read = true) : self
    {
        $participant = $this->getParticipantFromUser($userId);

        if ($participant !== null) {
            $participant->last_read = $read ? new \Carbon\Carbon() : null;
            $participant->save();
        }
        return $this;
    }

    public function markAsUnread(int $userId = null) : self
    {
        return $this->markAsRead($userId, false);
    }

    public function getParticipantFromUser(int $userId = null) : ?Participant
    {
        if ($userId === null) {
            return $this->participant;
        }
        return $this->participants->where('user_id', $userId)->first();
    }

    public function attachMessages($messages) : void
    {
        foreach ($messages as $message) {
            $this->attachMessage($message);
        }
    }

    public function attachMessage(Message $message) : Message
    {
        $this->messages()->save($message);

        return $message;
    }

    public function detachMessage($message) : ?Message
    {
        if (is_int($message) === true) {
            $message = $this->messages()->where('id', $message)->first();
        }

        if ($message instanceof Message) {
            try {
                $message->delete();
            } catch (\Exception $e) {}
        }

        return $message;
    }

    public function detachMessages($messages) : void
    {
        foreach ($messages as $message) {
            $this->detachMessage($message);
        }
    }

    public function attachParticipants($participants) : void
    {
        $this->participants()->saveMany($participants);
    }

    public function attachParticipant(Participant $participant) : Participant
    {
        $this->participants()->save($participant);

        return $participant;
    }

    public function detachParticipants($participants) : void
    {
        foreach ($participants as $participant) {
            $this->detachParticipant($participant);
        }
    }

    public function detachParticipant($participant) : ?Participant
    {
        if (is_int($participant) === true) {
            $participant = $this->participants()->where('user_id', $participant)->first();
        }

        if ($participant instanceof Participant) {
            try {
                $participant->delete();
            } catch (\Exception $e) {}
        }

        return $participant;
    }

}
