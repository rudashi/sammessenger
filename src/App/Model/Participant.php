<?php

namespace Totem\SamMessenger\App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int thread_id
 * @property int user_id
 * @property \Illuminate\Support\Carbon|null last_read
 */
class Participant extends Model
{
    use SoftDeletes;

    protected $dates = [
        'last_read',
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'updated_at',
            'deleted_at',
        ]);

        $this->fillable([
            'thread_id',
            'user_id',
            'last_read'
        ]);

        $this->setTable('message_users');

        parent::__construct($attributes);
    }

    public function thread() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('message_thread', 'thread_id', 'id');
    }

    public function user() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(app('config')->get('auth.providers.users.model'), 'user_id');
    }
}