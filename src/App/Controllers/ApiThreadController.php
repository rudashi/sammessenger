<?php

namespace Totem\SamMessenger\App\Controllers;

use Illuminate\Http\Request;
use Totem\SamMessenger\App\Requests\ThreadCreateRequest;
use Totem\SamMessenger\App\Resources\MessageResource;
use Totem\SamMessenger\App\Resources\Messenger\SimpleThreadResource;
use Totem\SamMessenger\App\Resources\ThreadResource;
use Totem\SamUsers\App\Resources\UserCollection;
use Totem\SamMessenger\App\Resources\ThreadCollection;
use Totem\SamMessenger\App\Repositories\Contracts\MessengerRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ApiThreadController extends ApiController
{

    public function __construct(MessengerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index() : ThreadCollection
    {
        return new ThreadCollection(
            $this->getFromRequestQuery($this->repository->getCurrentUserThreads())
        );
    }

    public function create(ThreadCreateRequest $request) : ThreadResource
    {
        return new ThreadResource($this->repository->storeThread($request));
    }

    public function show(string $slug) : ThreadResource
    {
        return new ThreadResource(
            $this->getFromRequestQuery($this->repository->findThreadWithRelations($slug, ['messages.user']))
        );
    }

    public function users(string $slug) : UserCollection
    {
        return new UserCollection(
            $this->getFromRequestQuery($this->repository->findThread($slug)->users->unique())
        );
    }

    public function update(string $slug, Request $request) : \Totem\SamCore\App\Resources\ApiResource
    {
        switch ($request->get('action')) {
            case 'read' :
                return new ThreadResource($this->repository->findThread($slug)->markAsRead(auth()->id()));
            case 'unread' :
                return new ThreadResource($this->repository->findThread($slug)->markAsUnread(auth()->id()));
            case 'archive' :
                return new ThreadResource($this->repository->markAsArchived($slug));
            default :
                return $this->response($this->error(422,'Missing Parameter.'));
        }
    }

    public function destroy(string $slug) : ThreadResource
    {
        return new ThreadResource(
            $this->repository->destroy($slug)
        );
    }

    public function latestMessage(string $slug): \Totem\SamCore\App\Resources\ApiResource
    {
        $message = $this->repository->findThread($slug)->latestMessage;

        if ($message !== null) {
            return new MessageResource($message->load('user'));
        }
        return $this->response($this->error(404,'Message not found'));
    }

    public function grouped(string $slug): SimpleThreadResource
    {
        return new SimpleThreadResource(
            $this->repository->fetchThread($slug)
        );
    }

}
