<?php

namespace Totem\SamMessenger\App\Controllers;

use Totem\SamMessenger\App\Requests\ParticipantRequest;
use Totem\SamMessenger\App\Resources\ParticipantResource;
use Totem\SamMessenger\App\Resources\ParticipantCollection;
use Totem\SamMessenger\App\Repositories\Contracts\MessengerRepositoryInterface;
use Totem\SamMessenger\App\Repositories\Contracts\ParticipantsRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ApiParticipantController extends ApiController
{

    private ParticipantsRepositoryInterface $participantsRepository;

    public function __construct(MessengerRepositoryInterface $repository, ParticipantsRepositoryInterface $participantsRepository)
    {
        $this->repository               = $repository;
        $this->participantsRepository   = $participantsRepository;
    }

    public function index(string $slug) : ParticipantCollection
    {
        return new ParticipantCollection(
            $this->getFromRequestQuery($this->repository->findThread($slug)->participants)
        );
    }

    public function create(string $slug, ParticipantRequest $request) : ParticipantResource
    {
        $thread = $this->repository->findThread($slug);
        $participant = $thread->getParticipantFromUser($request->input('user_id'));

        if ($participant === null) {
            $participant = $this->participantsRepository->add($thread, $request->input('user_id'), new \Carbon\Carbon());
        }

        return new ParticipantResource($participant);
    }

    public function destroy(string $slug, int $id) : ParticipantResource
    {
        $thread = $this->repository->findThread($slug);

        return new ParticipantResource(
            $this->participantsRepository->destroy($thread, $id)
        );
    }

}
