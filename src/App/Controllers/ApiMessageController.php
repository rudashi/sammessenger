<?php

namespace Totem\SamMessenger\App\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Totem\SamMessenger\App\Requests\MessageRequest;
use Totem\SamMessenger\App\Resources\MessageResource;
use Totem\SamMessenger\App\Resources\MessageCollection;
use Totem\SamMessenger\App\Resources\Messenger\PingResource;
use Totem\SamMessenger\App\Repositories\Contracts\MessagesRepositoryInterface;
use Totem\SamMessenger\App\Repositories\Contracts\MessengerRepositoryInterface;
use Totem\SamMessenger\App\Repositories\Contracts\ParticipantsRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ApiMessageController extends ApiController
{

    private MessagesRepositoryInterface $messagesRepository;
    private ParticipantsRepositoryInterface $participantsRepository;

    public function __construct(MessengerRepositoryInterface $repository, MessagesRepositoryInterface $messagesRepository, ParticipantsRepositoryInterface $participantsRepository)
    {
        $this->repository               = $repository;
        $this->messagesRepository       = $messagesRepository;
        $this->participantsRepository   = $participantsRepository;
    }

    public function index(string $slug) : MessageCollection
    {
        return new MessageCollection(
            $this->getFromRequestQuery($this->repository->findThreadWithRelations($slug, ['messages.user'])->messages)
        );
    }

    public function show(string $slug, int $id) : MessageResource
    {
        return new MessageResource(
            $this->repository->findThread($slug)->messages()->with('user')->where('id', $id)->first()
        );
    }

    public function create(string $slug, MessageRequest $request) : MessageResource
    {
        $thread = $this->repository->findThread($slug);

        if ($thread->locked) {
            throw new AccessDeniedHttpException(__('Unauthorized attempt to add a message.'));
        }

        if ($thread->getParticipantFromUser() === null) {
            $this->participantsRepository->add($thread, null, new \Carbon\Carbon());
        }

        return new MessageResource(
            $thread->attachMessage($this->messagesRepository->createModel($request))->load('user')
        );
    }

    public function replace(string $slug, int $id, MessageRequest $request) : MessageResource
    {
        $thread = $this->repository->findThread($slug);

        return new MessageResource(
            $this->messagesRepository->replace($thread, $id, $request->input('message'))
        );
    }

    public function destroy(string $slug, int $id) : MessageResource
    {
        $thread = $this->repository->findThread($slug);

        return new MessageResource(
            $this->messagesRepository->destroy($thread, $id)
        );
    }

    public function ping(Request $request)
    {
        if ($request->query('state') === "0") {
            return PingResource::collection(
                $this->messagesRepository->ping(
                    $request->post('__u', auth()->id()),
                    $request->post('__t', now()->format('Y-m-d H:i:s')),
                    $request->post('__s', [])
                )
            );
        }
        return $this->response($this->error(422,'Missing Parameter.'));
    }
}
