<?php

namespace Totem\SamMessenger;

use Illuminate\Support\Facades\File;
use Totem\SamMessenger\App\Model\Message;
use Totem\SamMessenger\App\Observers\MessageObserver;
use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;

class SamMessengerServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace() : string
    {
        return 'sam-messenger';
    }

    public function boot() : void
    {
        $this->loadAndPublish(
            __DIR__ . '/resources/lang',
            __DIR__ . '/database/migrations'
        );

        if (File::isDirectory(public_path('storage/messenger')) === false) {
            File::makeDirectory(public_path('storage/messenger'), $mode = 0777, true, true);
        }

        $this->loadRoutesFrom(__DIR__.'/routes/api.php');

        Message::observe(MessageObserver::class);
    }

    public function register() : void
    {
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');

        $this->configureBinding([
            \Totem\SamMessenger\App\Repositories\Contracts\MessengerRepositoryInterface::class => \Totem\SamMessenger\App\Repositories\MessengerRepository::class,
            \Totem\SamMessenger\App\Repositories\Contracts\MessagesRepositoryInterface::class => \Totem\SamMessenger\App\Repositories\MessagesRepository::class,
            \Totem\SamMessenger\App\Repositories\Contracts\ParticipantsRepositoryInterface::class => \Totem\SamMessenger\App\Repositories\ParticipantsRepository::class,
        ]);
    }

}
