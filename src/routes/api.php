<?php

use Illuminate\Support\Facades\Route;
use Totem\SamMessenger\App\Controllers\ApiThreadController;
use Totem\SamMessenger\App\Controllers\ApiMessageController;
use Totem\SamMessenger\App\Controllers\ApiParticipantController;

Route::group(['prefix' => 'api' ], static function() {

    Route::middleware(config('sam-admin.guard-api'))->group(static function() {

        Route::group(['prefix' => 'messenger', 'middleware' => 'permission:messenger.view'], static function() {

            Route::get('/', [ApiThreadController::class, 'index']);
            Route::get('{slug}', [ApiThreadController::class, 'show']);
            Route::get('{slug}/users', [ApiThreadController::class, 'users']);
            Route::get('{slug}/latest-message', [ApiThreadController::class, 'latestMessage']);

            Route::group(['prefix' => '{slug}/s'], static function() {
                Route::get('/', [ApiThreadController::class, 'grouped']);
            });

            Route::post('ping', [ApiMessageController::class, 'ping']);

            Route::post('/', [ApiThreadController::class, 'create']);
            Route::patch('{slug}', [ApiThreadController::class, 'update']);
            Route::delete('{slug}', [ApiThreadController::class, 'destroy'])->middleware('permission:messenger.delete');

            Route::get('{slug}/messages', [ApiMessageController::class, 'index']);
            Route::get('{slug}/messages/{id}', [ApiMessageController::class, 'show']);
            Route::post('{slug}/messages', [ApiMessageController::class, 'create']);
            Route::put('{slug}/messages/{id}', [ApiMessageController::class, 'replace'])->middleware('permission:messenger.edit');
            Route::delete('{slug}/messages/{id}', [ApiMessageController::class, 'destroy'])->middleware('permission:messenger.delete');

            Route::get('{slug}/participants', [ApiParticipantController::class, 'index']);
            Route::post('{slug}/participants', [ApiParticipantController::class, 'create']);
            Route::delete('{slug}/participants/{id}', [ApiParticipantController::class, 'destroy'])->middleware('permission:messenger.delete');
        });

    });

});
