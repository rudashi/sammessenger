Sam Messenger Component
================

This package manages internal messages in SAM applications.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

General System Requirements
-------------
- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-Admin ~1.*](https://bitbucket.org/rudashi/samadmin)
- [SAM-Users ~1.*](https://bitbucket.org/rudashi/samusers)  


Quick Installation
-------------
If necessary, use the composer to download the library

```
$ composer require totem-it/sam-messenger
```

Remember to put repository in a composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/sammessenger.git"
    }
],
```

Usage
-------------

###API

Endpoints use standard filtering from `SAM-core`.

Get All threads for logged user. 
```
GET /messanger

permission: messenger.view
```

Get thread with messages.
```
GET /messanger/{slug}

permission: messenger.view
```

Get users participants in the thread.
```
GET /messanger/{slug}/users

permission: messenger.view
```

Get the latest message from a thread.
```
GET /messanger/{slug}/latest-message

permission: messenger.view
```

Create a new thread. 
```
POST /messanger
{
    "subject": string,
    "start_date": string|optional,
    "end_date": string|optional,
    "max_participants": integer|optional,
    "avatar": string|nullable,
    "starred": integer,
}
permission: messanger.view
```

Mark thread as read | unread
```
PATCH /messanger/{slug}
{
   "action":”read|unread|archive”
}

permission: messanger.view
```

Delete thread. 
```
DELETE /messanger/{slug}

permission: messanger.delete
```

Get thread messages.
```
GET /messanger/{slug}/messages

permission: messenger.view
```

Get thread message by an id.
```
GET /messanger/{slug}/messages/{id}

permission: messenger.view
```

Create a new message in thread
```
POST /messanger/{slug}/messages
{
    "message": string,
    "notify": array|optional
}
permission: messenger.view
```

Replace a message in the thread.
```
GET /messanger/{slug}/messages/{id}

permission: messenger.edit
```

Delete a message.
```
DELETE /messanger/{slug}/messages/{id}

permission: messenger.delete
```

Get thread participants.
```
GET /messanger/{slug}/participants

permission: messenger.view
```

Add new participant in thread
```
POST /messanger/{slug}/participants
{
    "user_id": integer,
}
permission: messenger.view
```

Delete participant from the thread.
```
DELETE /messanger/{slug}/participants/{id}

permission: messenger.delete
```

Ping for new messages.
```
POST /messanger/ping?state=0
{
    "__u": integer|user
    "__t": date_time
    "__s": array|slugs
}

permission: messenger.view

```

Authors
-------------

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
